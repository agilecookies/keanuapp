import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FavouriteComponent } from './components/favourite/favourite.component';
import { MainComponent } from './components/main/main.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ScoresComponent } from './components/scores/scores.component';
import { UploadComponent } from './components/upload/upload.component';
import { VotingComponent } from './components/voting/voting.component';
import { MaterialModule } from './material/material.module';
import { CatsService } from './services/cats.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UploadComponent,
    FavouriteComponent,
    VotingComponent,
    ScoresComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  providers: [CatsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
