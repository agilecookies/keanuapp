import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';


const Material = [
  MatButtonModule,
  MatToolbarModule,
  MatIconModule,
  MatSnackBarModule
];

@NgModule({
  imports: [Material],
  exports: [Material],
})
export class MaterialModule {}
