import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from './../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class CatsService {
  private API_URL = environment.API_URL;
  private httpOptions = {
    headers: new HttpHeaders({
      'X-Api-Key': environment.API_KEY
    })
  };
  constructor(private http: HttpClient) { }

  getCats(): Observable<any>{
    return this.http.get(`${environment.API_URL}images?limit=50`, this.httpOptions);
  }
  uploadCatImage(fileData): Observable<any> {
    return this.http.post(`${environment.API_URL}images/upload`, fileData , this.httpOptions );
  }
  getFavs(): Observable<any>{
    return this.http.get(`${environment.API_URL}favourites`, this.httpOptions);
  }
  addFavs(catId): Observable<any> {
    return this.http.post(`${environment.API_URL}favourites`, {image_id: catId}, this.httpOptions);
  }
  deleteFav(catId): Observable<any> {
    return this.http.delete(`${environment.API_URL}favourites/${catId}`, this.httpOptions);
  }
  voteUp(catId): Observable<any> {
    return this.http.post(`${environment.API_URL}votes`, {image_id: catId, value: 1}, this.httpOptions);
  }
  voteDown(catId): Observable<any> {
    return this.http.post(`${environment.API_URL}votes`, {image_id: catId, value: 0}, this.httpOptions);
  }
  getVotes(): Observable<any> {
    return this.http.get(`${environment.API_URL}votes?limit=500`, this.httpOptions);
  }
}
