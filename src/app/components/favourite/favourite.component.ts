import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { FavCat } from '../../models/fav-cats.interface';

@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.scss']
})

export class FavouriteComponent implements OnInit {
  @Input() catId;
  @Input() favCats: [FavCat];
  @Output() favStatus: EventEmitter<any>  = new EventEmitter();

  public isFav = false;
  public favId: number;

  constructor() { }

  ngOnInit(): void {
   Object.entries(this.favCats).forEach(([key, value]) => {
     if (value.image_id === this.catId) {
        this.isFav = true;
        this.favId = value.id;
      }
   });
  }
  togglFav(favId, catId): void {
    if (favId) {
      this.favStatus.emit(favId);
    } else {
      this.favStatus.emit(catId);
    }
  }
}
