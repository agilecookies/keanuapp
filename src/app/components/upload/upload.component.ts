import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

import { CatsService } from './../../services/cats.service';



@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
  public fileData: File;
  public previewUrl: any;
  public showError = false;

imageForm = new FormGroup({
  image: new FormControl('', Validators.required)
});
constructor(
  private catsService: CatsService,
  private router: Router,
  private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
  }


preview(fileInput: any): void {
    this.showError = false;
    this.fileData = ( fileInput.target.files[0] as File);
    const mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (event) => {
      this.previewUrl = reader.result;
    };
}

onSubmit(): void {
    const formData = new FormData();
    formData.append('file', this.fileData);
    this.catsService.uploadCatImage(formData)
        .subscribe(res => {
          this.snackBar.open('Image Uploaded', 'OK', {
            duration: 2000
          });
          this.router.navigate(['/']);
        },
        error => {
          this.showError = true;
          this.imageForm.reset();
          this.previewUrl = null;
        });
}
}
