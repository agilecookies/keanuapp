import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-voting',
  templateUrl: './voting.component.html',
  styleUrls: ['./voting.component.scss']
})
export class VotingComponent implements OnInit {
  @Input() catId;
  @Output() voteUpevent: EventEmitter<any> = new EventEmitter();
  @Output() voteDownevent: EventEmitter<any> = new EventEmitter();

  public votedUp = false;
  public votedDown = false;

  constructor() { }

  ngOnInit(): void {
  }

  voteUp(catId): void{
    this.votedUp = true;
    this.votedDown = false;
    this.voteUpevent.emit(catId);
  }
  voteDown(catId): void {
    this.votedDown = true;
    this.votedUp = false;
    this.voteDownevent.emit(catId);
  }
}
