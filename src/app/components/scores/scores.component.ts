import { Component, Input, OnInit } from '@angular/core';

import { Score } from '../../models/score.inteface';

@Component({
  selector: 'app-scores',
  templateUrl: './scores.component.html',
  styleUrls: ['./scores.component.scss']
})
export class ScoresComponent implements OnInit {
  @Input() score: [Score];
  @Input() catId: string;
  public totalScore = 0;

  constructor() { }

  ngOnInit(): void {
   this.getScore();
  }
  getScore(): void{
    Object.entries(this.score).forEach(([key, value]) => {
      if (value.image_id === this.catId) {
       switch (value.value){
        case 0:
          this.totalScore--;
          break;
        case 1:
         this.totalScore++;
         break;
        default:
          break;
      }
       }
    });
  }
}
