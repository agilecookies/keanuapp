import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import { Data } from '../../models/fav-cats.interface';
import { CatsService } from './../../services/cats.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  public data: Data[];
  public loading = true;
  public catId: string;
  public favCats: any;
  public votes: any;


  constructor(
    private catService: CatsService,
    private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.updateData();
  }


  getCats(): void{
    this.catService.getCats().subscribe((res) => {
      this.data = res;
      this.loading = false;
   });
  }

  getFavs(): void {
    this.catService.getFavs().subscribe(res => {
    this.favCats = res;
    });
  }

  getVotes(): void {
    this.catService.getVotes().subscribe( res => {
    this.votes = res;
    });
  }
  toggleFav(id): void{
    // tslint:disable-next-line: triple-equals
    if (typeof id == 'number'){
      this.catService.deleteFav(id).subscribe(res => {
        this.updateData();
      },
      error => {
        console.log(error);
      }
      );
    } else {
        this.catService.addFavs(id).subscribe(res => {
          this.updateData();
        });
    }
  }
  voteUp(catId): void {
    this.catService.voteUp(catId).subscribe(res => {
      this.updateData();
    });
  }
  voteDown(catId): void {
    this.catService.voteDown(catId).subscribe(res => {
      this.updateData();
    });

  }
  updateData(): void {
    this.getFavs();
    this.getVotes();
    this.getCats();
  }

}
