export interface FavCat {
    id: number;
    user_id: string;
    image_id: string;
    sub_id?: any;
  }
export interface Data {
    breeds: [];
    id: string;
    url: string;
    width: number;
    height: number;
    sub_id: null;
    created_at?: any;
    original_filename?: string;
    breed_ids?: any;
}
