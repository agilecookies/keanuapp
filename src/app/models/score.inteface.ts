export interface Score{
    id: number;
    user_id: string;
    image_id: string;
    sub_id?: any;
    value: number;
}

